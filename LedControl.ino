#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <SoftwareSerial.h>
#define LED_PIN 4
#define LED_PIN_DOME 3
#define NUM_LEDS 13
#define NUM_LEDS_DOME 20

// put function declarations here:
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel domeStrip = Adafruit_NeoPixel(NUM_LEDS_DOME, LED_PIN_DOME, NEO_GRB + NEO_KHZ800);

void Control_From_Serial(void);
void Brightness_Mode(void);
void Brightness_Mode_Dome(void);
SoftwareSerial mySerial(1, 2);


void setup()
{
	strip.begin();
	strip.setBrightness(100);
	strip.show(); // Initialize all LEDs to off


	domeStrip.begin();
	domeStrip.setBrightness(100);
	domeStrip.show();

  // strip.fill(strip.Color(255, 255, 255));
  // strip.show();

  // domeStrip.fill(strip.Color(255, 255, 255));
  // domeStrip.show();

	mySerial.begin(19200);

}

void loop()
{
	if (mySerial.available())
		Control_From_Serial();
 
  delay(500);
}

void Control_From_Serial(void)
{
	char receivedChar;

	while (mySerial.read() >= 0) // Flush any remain char
		;
	while (1)
	{
		if (mySerial.available())
		{
			receivedChar = mySerial.read();
			switch (receivedChar)
			{
			case 'a':
				strip.clear();
				strip.setPixelColor(0, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				strip.setPixelColor(1, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				strip.setPixelColor(2, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				strip.show();
				break;

			case 'b':
				strip.clear();
				strip.setPixelColor(3, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				strip.setPixelColor(4, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				strip.setPixelColor(5, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				strip.show();
				break;

			case 'c':
				strip.clear();
				strip.setPixelColor(6, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				strip.setPixelColor(7, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				strip.setPixelColor(8, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				strip.show();
				break;

			case 'd':
				strip.clear();
				strip.setPixelColor(9, strip.Color(255, 255, 255));	 // White: (255, 255, 255)
				strip.setPixelColor(10, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				strip.setPixelColor(11, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				strip.show();
				break;

			case 'e':
				strip.fill(strip.Color(255, 255, 255));
				strip.show();
				break;

			case 'f': // dome 1
				domeStrip.clear();
				domeStrip.setPixelColor(0, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(1, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(2, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(3, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(4, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.show();
				break;

			case 'g': // dome 2
				domeStrip.clear();
				domeStrip.setPixelColor(5, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(6, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(7, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(8, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(9, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.show();
				break;

			case 'h': // dome 3
				domeStrip.clear();
				domeStrip.setPixelColor(10, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(11, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(12, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(13, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(14, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.show();
				break;

			case 'i': // dome 4
				domeStrip.clear();
				domeStrip.setPixelColor(15, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(16, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(17, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(18, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.setPixelColor(19, strip.Color(255, 255, 255)); // White: (255, 255, 255)
				domeStrip.show();
				break;

			case 'j': // dome all
				domeStrip.fill(strip.Color(255, 255, 255));
				domeStrip.show();
				break;

			case 'l':
				Brightness_Mode();
				break;

			case 'k': // dome bright
				Brightness_Mode_Dome();
				break;

			default:
				return;
				break;
			}
		}
    // delay(200);
	}
}

void Brightness_Mode(void)
{
	uint8_t brightness = 0;
	String inputString;

	// mySerial.print("\nPlease input Led Brightness: 1~255\n");
	while (mySerial.read() >= 0)
		; // Flush any extra characters
	while (!mySerial.available())
		; // Wait for User input

	inputString = mySerial.readStringUntil('\n'); // read the input string until a newline character is received
	brightness = inputString.toInt();

	if (brightness > 0)
	{
		strip.setBrightness(brightness);
		strip.show();
	}
	else
	{
		// mySerial.print("\nError, Please input a value between 1~255");
	}
}

void Brightness_Mode_Dome(void)
{
	uint8_t brightness = 0;
	String inputString;

	// mySerial.print("\nPlease input Led Brightness: 1~255\n");
	while (mySerial.read() >= 0)
		; // Flush any extra characters
	while (!mySerial.available())
		; // Wait for User input

	inputString = mySerial.readStringUntil('\n'); // read the input string until a newline character is received
	brightness = inputString.toInt();

	if (brightness > 0)
	{
		domeStrip.setBrightness(brightness);
		domeStrip.show();
	}
	else
	{
		// mySerial.print("\nError, Please input a value between 1~255");
	}
}
